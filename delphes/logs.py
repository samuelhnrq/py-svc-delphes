from logging import config

# import os


def setup_logging():
    config.dictConfig(
        {
            "version": 1,
            "formatters": {
                "default": {
                    "format": "[%(asctime)s][%(levelname)s][%(module)s][%(name)s] %(message)s"
                },
                "werkzeug": {
                    "format": "[%(levelname)s][%(name)s] %(message)s",
                },
            },
            "handlers": {
                "standard": {
                    "class": "google.cloud.logging.handlers.StructuredLogHandler",
                },
                "werkzeug": {
                    "class": "logging.StreamHandler",
                    "stream": "ext://sys.stdout",
                    "formatter": "werkzeug",
                },
            },
            "loggers": {
                "werkzeug": {
                    "level": "INFO",
                    "handlers": ["werkzeug"],
                    "propagate": False,
                }
            },
            "root": {"level": "INFO", "handlers": ["standard"]},
        }
    )
