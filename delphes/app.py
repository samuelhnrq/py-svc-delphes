import logging
from eve import Eve
from flask import Request

from . import settings, logs

logs.setup_logging()
logger = logging.getLogger(__name__)
app = Eve(settings=settings.__file__)


def global_resource_hook(resource, req: Request, _lookup=None):
    logger.info("Got an '%s' call on, '%s' resource", req.method, resource)


verbs = ["GET", "HEAD", "POST", "PUT", "DELETE", "PATCH"]
for verb in verbs:
    setattr(app, f"on_pre_{verb}", global_resource_hook)

logger.info("Setup flask app successfully")
