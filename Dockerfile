FROM python:3.10-alpine

RUN apk add build-base && \
  pip install --no-cache-dir poetry==1.2.* && \
  adduser -h /app -s /bin/sh -S appuser

USER appuser
WORKDIR /app

COPY poetry.lock pyproject.toml /app/
RUN poetry config virtualenvs.in-project true && \
  poetry install --without=dev --no-cache

COPY ./gunicorn.conf.py /app
COPY ./delphes /app/delphes/

ENV PYTHONUNBUFFERED=True \
  OTEL_PYTHON_LOG_CORRELATION=true \
  OTEL_TRACES_EXPORTER=gcp_trace \
  OTEL_PROPAGATORS=gcp_trace \
  OTEL_METRICS_EXPORTER=none

CMD poetry run opentelemetry-instrument gunicorn
